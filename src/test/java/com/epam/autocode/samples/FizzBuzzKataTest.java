package com.epam.autocode.samples;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.concurrent.TimeUnit;

class FizzBuzzKataTest {
    FizzBuzzKata fizzBuzzKata = new FizzBuzzKata();

    @Test
    void testCheckFizzBuzz() throws InterruptedException {
        System.err.println("START SLEEP");
        TimeUnit.MINUTES.sleep(8);
        System.err.println("END SLEEP");
        String result = fizzBuzzKata.checkFizzBuzz(0);
        Assertions.assertEquals("replaceMeWithExpectedResult", result);
    }
}